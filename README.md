# Braces
Informační systém pro společnost ***BlueTravel s.r.o.***

### Návod na spuštění
- Projekt je potřeba naimportovat jako ***Maven project*** do vývojového prostředí.
- Je potřeba mít nainstalovaný MySQL server ve verzi min. 5.
- V MySQL je potřeba mít vytvořené schéma s názvem ***braces*** s porovnáváním ***utf8_general_ci***.
- V souboru ***application.properties***, potom nastavit přístupové údaje pro datasource. Pokud je použito výchozí nastavení MySQL, tak pouze uživatel a heslo.