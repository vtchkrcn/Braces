<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zájezdy &middot; BlueTravel s.r.o. &middot; BRACES</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
  </head>
<body>

	<header class="row headrow">
      <div class="container confirt">
         <div class="col-md-4 left">
         	<a class="bqc brd" href="/">
            	<img border="0" alt="Domů" src="img/lolgo.png" width="200" height="75">
            </a>
         </div>
         <div class="col-md-4">
           <a href="#"><img src="img/facebook_32.png" alt="facebook" class="img-rounded img-responsive leftsocail"></a>
           <a href="#"><img src="img/twitter_32.png" alt="twitter" class="img-rounded img-responsive leftsocail"></a>
           <a href="#"><img src="img/rss_32.png" alt="twitter" class="img-rounded img-responsive leftsocail"></a>
         </div>
         <div class="col-md-4 right">  
              <form class="navbar-form navbar-right" role="search">
                  <div class="form-group">
                     <input type="text" class="form-control" placeholder="Hledat...">
                  </div>
                      <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
               </form>
				<br>

          </div>
       </div>
    </header>
     <div class="row headrowmenu">
       	<div class="container">
        	<nav class="navbar navbar-inverse " role="navigation">
               <ul class="nav nav-pills pilledit">
                  <li class="active"><a href="/"><span class="glyphicon glyphicon-home"> Home</span></a></li>
                  <li><a href="/trips">Last minute</a></li>
                  <li><a href="/trips">Za mořem</a></li>
                  <li><a href="/trips">Za poznáním</a></li>
                  <li><a href="/trips">Relax</a></li>
                  <li><a href="/trips">Akce</a></li>
               </ul>
         	</nav>  
         </div>  
     </div>
    
    <div class="row excontr">
       <div class="container">
       <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            </ol>
          
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="img/city.jpg" alt="Responsive image" class="img-responsive">
                <div class="carousel-caption">
                  <h1>Business cesta do Londýna</h1>
                  <p>jen za 15 000 Kč</p>
                </div>
              </div>
            <div class="item">
                 <img src="img/citynice.jpg" alt="Responsive image" class="img-responsive">
                <div class="carousel-caption">
                  <h1>Poznejte krásy New Yorku</h1>
                  <p>pouze 35 000 Kč</p>
                </div>
              </div>
            </div>
             
          
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
          </div>
         </div>
    </div>
    
    <div class="row">
    	<div class="container marketing">
        	<h1>Zájezdy</h1>
           	<div class="box" style="height: 1150px;">
           	
           	<c:forEach items="${trips}" var="trip">
	           	<div class="col-lg-4">
                    <a href="#">
                    	<img class="img-circle" src="img/universal1.jpg" width="100" height="100"/>
                    </a>
                	<h4><c:out value="${trip.destination}" /></h4>
                 	<button type="button" onclick="window.location.href='/contract/new'" class="btn btn-primary btn-sm">Objednat</button>
                	<h5><c:out value="${trip.totalPrice}" /> Kč</h5>
			      	<p style="font-size: 15px">
				        <c:out value="${trip.description}" />
				        <a href="#" style="text-decoration: none; color: inherit">čti více</a>
				    </p>
           		</div><!-- /.col-lg-4 -->
			</c:forEach>

       		</div>
        </div>
    </div>
    
    <div class="row extralit">
         <div class="container">
             <div class="col-lg-4">
               <div class="list-group">
                <a href="#" class="list-group-item active">
                  <h3>Zájezdy</h3>
                </a>
                <a href="#" class="list-group-item">Last minute</a>
                <a href="#" class="list-group-item">Za mořem</a>
                <a href="#" class="list-group-item">Za poznáním</a>
                <a href="#" class="list-group-item">Akce</a>
               </div>
             </div>
             <div class="col-lg-4">
               <div class="list-group">
                <a href="#" class="list-group-item active">
                  <h3>Podrobnosti</h3>
                </a>
                <a href="#" class="list-group-item">Přihlášení pro partnery</a>
                <a href="#" class="list-group-item">Přihlášení pro klienty</a>
                <a href="#" class="list-group-item">Často kladené otázky</a>
                <a href="#" class="list-group-item">Info o destinacích</a>
               </div>
             </div>
             <div class="col-lg-4">
               <div class="list-group">
                <a href="#" class="list-group-item active">
                  <h3>Naše společnost</h3>
                </a>
                <a href="#" class="list-group-item">O společnosti</a>
                <a href="#" class="list-group-item">Kontaktujte nás</a>
                <a href="#" class="list-group-item">Smluvní podmínky</a>
                <a href="#" class="list-group-item">Podpora</a>
               </div>
             </div>
          </div>
    </div>
    <footer class="row extrafooter">
      <div class="container">
         <div class="col-md-6">Copyright @ 2018 .ALl Right Reseved.</div>
         <div class="col-md-4 right">Design & Developed By: Arcos</div>
      </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.js"></script>
	
</body>
</html>
