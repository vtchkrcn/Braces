<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rezervace &middot; BlueTravel s.r.o. &middot; BRACES</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet">
  </head>
<body>
	<header class="row headrow">
      <div class="container confirt">
         <div class="col-md-4 left">
			<a class="bqc brd" href="/">
            	<img border="0" alt="Domů" src="../img/lolgo.png" width="200" height="75">
            </a>
         </div>
         <div class="col-md-4">
         	<a href="#"><img src="../img/facebook_32.png" alt="facebook" class="img-rounded img-responsive leftsocail"></a>
         	<a href="#"><img src="../img/twitter_32.png" alt="twitter" class="img-rounded img-responsive leftsocail"></a>
            <a href="#"><img src="../img/rss_32.png" alt="twitter" class="img-rounded img-responsive leftsocail"></a>
         </div>
         <div class="col-md-4 right">  
              <form class="navbar-form navbar-right" role="search">
                  <div class="form-group">
                     <input type="text" class="form-control" placeholder="Hledat...">
                  </div>
                  <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
               </form>
				<br>
          </div>
       </div>
    </header>
    
    <div class="row headrowmenu">
       	<div class="container">
        	<nav class="navbar navbar-inverse " role="navigation">
                <ul class="nav nav-pills pilledit">
                   <li><a href="/"><span class="glyphicon glyphicon-home"> Home</span></a></li>
                   <li class="active"><a href="/trips">Last minute</a></li>
                   <li><a href="/trips">Za mořem</a></li>
                   <li><a href="/trips">Za poznáním</a></li>
                   <li><a href="/trips">Relax</a></li>
                   <li><a href="/trips">Akce</a></li>
                </ul>
            </nav>
        </div>  
    </div>
    
    <div class="row excontr">
       <div class="container">
       <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
             
            </ol>
          
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="../img/city.jpg" alt="Responsive image" class="img-responsive">
                <div class="carousel-caption">
                  <h1>Business cesta do Londýna</h1>
                  <p>jen za 15 000 Kč</p>
                </div>
              </div>
            <div class="item">
                 <img src="../img/citynice.jpg" alt="Responsive image" class="img-responsive">
                <div class="carousel-caption">
                  <h1>Poznejte krásy New Yorku</h1>
                  <p>pouze 35 000 Kč</p>
                </div>
              </div>
            </div>
             
          
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
          </div>
         </div>
    </div>
    <div class="row">
    	<div class="container">
    	
			<h1>Rezervace zájezdu: ${contract.trip.destination}</h1>
            <div class="box">
				<div class="validation-summary-valid" data-valmsg-summary="true">
					<ul><li style="display:none"></li></ul>
				</div>

				<form:form action="/contract/new" modelAttribute="contract" class="form-horizontal" enctype="multipart/form-data" method="post">    
			     	<div class="form-group ">
			       		<label class="col-xs-3 col-md-2 control-label">Začátek pobytu</label>
			      		<div class="col-xs-8"><fmt:formatDate value="${contract.trip.dateStart}" pattern="dd.MM.yyyy HH:mm" /></div>
			     	</div>
			     	
			     	<div class="form-group ">
			       		<label class="col-xs-3 col-md-2 control-label">Konec pobytu</label>
			      		<div class="col-xs-8"><fmt:formatDate value="${contract.trip.dateEnd}" pattern="dd.MM.yyyy HH:mm" /></div>
			     	</div>
			    
				    <div class="form-group">
				        <label class="col-xs-3 col-md-2 control-label">Jméno</label>
				        <div class="col-xs-8">
				            <form:input class="form-control" data-val="true" data-val-required="Jméno je vyžadováno!" path="firstName" type="text" required="required" />
				            <span class="field-validation-valid" data-valmsg-for="firstName" data-valmsg-replace="true">${messages.firstName}</span>
				        </div>
				    </div>
				    
				    <div class="form-group">
				        <label class="col-xs-3 col-md-2 control-label">Příjmení</label>
				        <div class="col-xs-8">
				            <form:input class="form-control" data-val="true" data-val-required="Příjmení je vyžadováno!" path="lastName" type="text" required="required" />
				            <span class="field-validation-valid" data-valmsg-for="lastName" data-valmsg-replace="true">${messages.lastName}</span>
				        </div>
				    </div>
				    
			        <div class="form-group">
			        	<label class="col-xs-3 col-md-2 control-label">E-mail</label>
			        	<div class="col-xs-8">
			            	<form:input class="form-control" data-val="true" data-val-email="Zadejte Email ve správném tvaru!"  path="email" placeholder="email@braces.cz" type="email" value="" required="required" />
			            	<span class="field-validation-valid" data-valmsg-for="email" data-valmsg-replace="true">${messages.email}</span>
				        </div>
				    </div>
			        
			        <div class="form-group">
				        <label class="col-xs-3 col-md-2 control-label">Telefoní číslo</label>
				        <div class="col-xs-8">
				            <form:input class="form-control" path="phone" type="text" value="" required="required" />
				            <span class="field-validation-valid" data-valmsg-for="phone" data-valmsg-replace="true">${messages.phone}</span>
				        </div>
			   		</div>
			    	
				    <div class="form-group">
				        <label class="col-xs-3 col-md-2 control-label">Ulice</label>
				        <div class="col-xs-8">
				            <form:input class="form-control" data-val="true" data-val-required="Název ulice je vyžadován" path="street" type="text" value="" required="required" />
				            <span class="field-validation-valid" data-valmsg-for="street" data-valmsg-replace="true">${messages.street}</span>
				        </div>
				    </div>
			    
				    <div class="form-group">
				        <label class="col-xs-3 col-md-2 control-label">Číslo popisné</label>
				        <div class="col-xs-8">
				            <form:input class="form-control" data-val="true" data-val-number="Číslo popisné musí být číslo" data-val-required="Číslo popisné je vyžadováno" path="houseNumber" type="text" value="" required="required" />
				            <span class="field-validation-valid" data-valmsg-for="houseNumber" data-valmsg-replace="true">${messages.houseNumber}</span>
				        </div>
				    </div>
				    
				    <div class="form-group">
				        <label class="col-xs-3 col-md-2 control-label">PSČ</label>
				        <div class="col-xs-8">
				            <form:input class="form-control" data-val="true" data-val-number="PSČ musí být číslo" data-val-required="Číslo popisné je vyžadováno" path="zip" type="text" required="required" />
				            <span class="field-validation-valid" data-valmsg-for="zip" data-valmsg-replace="true">${messages.zip}</span>
				        </div>
				    </div>
				    
			        <div class="form-group">
				        <label class="col-xs-3 col-md-2 control-label">Město</label>
				        <div class="col-xs-8">
				            <form:input class="form-control" data-val="true" data-val-required="Město je vyžadováno" path="city" type="text" value="" required="required" />
				            <span class="field-validation-valid" data-valmsg-for="city" data-valmsg-replace="true">${messages.city}</span>
				        </div>
				    </div>
				    
				    <form:hidden path="trip"/>
				    
				    <div class="form-group">
				        <label style="color:red" class="col-xs-3 col-md-2 control-label">Cena: ${contract.trip.totalPrice} Kč</label>
				    </div>
			    
				    <div class="form-group">
				        <div class="col-sm-offset-2 col-sm-6">
				            <button class="btn btn-default">Rezervovat</button>
				            <a href="/#content" class="btn btn-default">Zrušit</a>
				        </div>
				    </div>
				</form:form>
			</div>  
		</div>
    </div>
    
    <div class="row extralit">
         <div class="container">
             <div class="col-lg-4">
               <div class="list-group">
                <a href="#" class="list-group-item active">
                  <h3>Zájezdy</h3>
                </a>
                <a href="#" class="list-group-item">Last minute</a>
                <a href="#" class="list-group-item">Za mořem</a>
                <a href="#" class="list-group-item">Za poznáním</a>
                <a href="#" class="list-group-item">Akce</a>
               </div>
             </div>
             <div class="col-lg-4">
               <div class="list-group">
                <a href="#" class="list-group-item active">
                  <h3>Podrobnosti</h3>
                </a>
                <a href="#" class="list-group-item">Přihlášení pro partnery</a>
                <a href="#" class="list-group-item">Přihlášení pro klienty</a>
                <a href="#" class="list-group-item">Často kladené otázky</a>
                <a href="#" class="list-group-item">Info o destinacích</a>
               </div>
             </div>
             <div class="col-lg-4">
               <div class="list-group">
                <a href="#" class="list-group-item active">
                  <h3>Naše společnost</h3>
                </a>
                <a href="#" class="list-group-item">O společnosti</a>
                <a href="#" class="list-group-item">Kontaktujte nás</a>
                <a href="#" class="list-group-item">Smluvní podmínky</a>
                <a href="#" class="list-group-item">Podpora</a>
               </div>
             </div>
          </div>
    </div>
    <footer class="row extrafooter">
      <div class="container">
         <div class="col-md-6">Copyright @ 2018 .ALl Right Reseved.</div>
         <div class="col-md-4 right">Design & Developed By: Arcos</div>
      </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap.js"></script> 
    <script src="../js/bootstrap-datepicker.min.js?t=20130302"></script>
	
</body>
</html>
