package cz.uhk.fim.mois.braces.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.uhk.fim.mois.braces.model.Contract;
import cz.uhk.fim.mois.braces.repository.ContractRepository;
import cz.uhk.fim.mois.braces.service.ContractService;

@Service
public class ContractServiceImpl implements ContractService {

	@Autowired
	private ContractRepository contractRepository;
	
	@Override
	public void save(Contract contract) {
		contractRepository.save(contract);
	}

	@Override
	public Optional<Contract> findById(Integer id) {
		return contractRepository.findById(id);
	}

}
