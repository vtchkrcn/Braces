package cz.uhk.fim.mois.braces.service;

import java.util.List;
import java.util.Optional;

import cz.uhk.fim.mois.braces.model.Trip;

public interface TripService {
	
	public Optional<Trip> findById(Integer id);
	
	public List<Trip> findAll();

}
