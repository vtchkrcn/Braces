package cz.uhk.fim.mois.braces.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cz.uhk.fim.mois.braces.model.Contract;

@Repository
public interface ContractRepository extends CrudRepository<Contract, Integer> {
	

}
