package cz.uhk.fim.mois.braces.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import cz.uhk.fim.mois.braces.service.TripService;

@Controller
public class TripController {

	@Autowired
	private TripService tripService;
	
	@GetMapping("/trips")
	public String initTrips(Model model) {
		model.addAttribute("trips", tripService.findAll());
		return "/trip/list";
	}
}
