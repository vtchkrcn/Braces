package cz.uhk.fim.mois.braces.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cz.uhk.fim.mois.braces.model.Trip;

@Repository
public interface TripRepository extends CrudRepository<Trip, Integer> {
	
}
