package cz.uhk.fim.mois.braces.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cz.uhk.fim.mois.braces.model.Trip;
import cz.uhk.fim.mois.braces.repository.TripRepository;
import cz.uhk.fim.mois.braces.service.TripService;

@Repository
public class TripServiceImpl implements TripService {
	
	@Autowired
	TripRepository tripRepository;

	@Override
	public Optional<Trip> findById(Integer id) {
		return tripRepository.findById(id);
	}

	@Override
	public List<Trip> findAll() {
		return (List<Trip>) tripRepository.findAll();
	}
	
}
