package cz.uhk.fim.mois.braces.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="trip")
public class Trip extends BaseEntity {

	@Column(name = "destination")
	@NotNull
	private String destination;
	
	@Column(name = "total_price")
	@NotNull
	private Integer totalPrice;
	
	@Column(name = "rooms")
	@NotNull
	private Integer rooms;
	
	@Column(name = "date_start")
	@NotNull
	private Date dateStart;
	
	@Column(name = "date_end")
	@NotNull
	private Date dateEnd;
	
	@Column(name = "description", columnDefinition = "TEXT")
	@NotNull
	private String description;
	
	public Trip() {
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Integer getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
