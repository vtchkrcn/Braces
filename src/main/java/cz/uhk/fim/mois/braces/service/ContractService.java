package cz.uhk.fim.mois.braces.service;

import java.util.Optional;

import cz.uhk.fim.mois.braces.model.Contract;

public interface ContractService {

	public void save(Contract contract);

	public Optional<Contract> findById(Integer id);
}
