package cz.uhk.fim.mois.braces.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import cz.uhk.fim.mois.braces.model.Contract;
import cz.uhk.fim.mois.braces.service.ContractService;
import cz.uhk.fim.mois.braces.service.TripService;

@Controller
public class ContractController {
	
	@Autowired
	private ContractService contractService;
	
	@Autowired
	private TripService tripService;
	
	@GetMapping("/contract/new")
	public String initNew(Model model) {
		Contract contract = new Contract();
		contract.setTrip(tripService.findById(1).get());
		model.addAttribute("contract", contract);
		return "contract/edit";
	}
	
	@PostMapping("/contract/new")
	public String processNew(@Valid Contract contract, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "contract/edit";
		}
		contractService.save(contract);
		return "redirect:/";
	}
}
