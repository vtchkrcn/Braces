package cz.uhk.fim.mois.braces;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BracesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BracesApplication.class, args);
	}
}
